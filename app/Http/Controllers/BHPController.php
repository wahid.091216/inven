<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang_hp;
use App\Pengambilan;
use PDF;
use Excel;
use Illuminate\Support\Facades\Input;

class BHPController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index(Barang_hp $bar)
    {
      $bar = Barang_hp::all();
      // dd($barang);
      return view('barang/index', ['bar' => $bar]);
    }

    public function detail(Barang_hp $barang, Pengambilan $pengambil, $naba)
    {
      $barang = Barang_hp::where('naba',  $naba)->get();
      $pengambil = Pengambilan::where('barang', 'like', $naba."%")->get();
      return view('barang_hp/detail', ['barang' => $barang, 'pengambil' => $pengambil]);
    }

    public function create()
    {
      # code...
      return view('barang_hp/tambah');
    }

    public function store(Request $req, Barang_hp $bar)
    {
      # code...
      $b = new $bar;
      $b->naba = $req->nama;
      $b->merk = $req->merk;
      $b->koba = $req->kode;
      $b->harga = $req->harga;
      $b->tanggal = $req->tgl;
      $b->juwal = $req->awal;
      $b->jukhir = $req->akhir;
      $b->ket = $req->ket;

      $b->save();

      return redirect('barang/index');
    }

    public function edit(barang_hp $bar, $id)
    {
      # code...
      $bar = Barang_hp::findOrFail($id);
      return view('barang_hp/ubah', ['bar' => $bar]);
    }

    public function update(Request $req, $id)
    {
      # code...
      $b = Barang_hp::findOrFail($id);
      $b->naba = $req->nama;
      $b->merk = $req->merk;
      $b->koba = $req->kode;
      $b->harga = $req->harga;
      $b->tanggal = $req->tgl;
      $b->juwal = $req->awal;
      $b->jukhir = $req->akhir;
      $b->ket = $req->ket;

      $b->save();

      return redirect ('barang/index');
    }

    public function delete(Barang_hp $b, $id)
    {
      # code...
      $b = Barang_hp::findOrFail($id);
      $b->delete();

      return redirect('barang/index');
    }

    public function pdf()
    {
      $bar = Barang_hp::all()->where('jukhir','<=','1');
      $pdf = PDF::loadView('barang_hp.pdf', ['bar' => $bar]);
      return $pdf->stream('barang_hp.pdf');
    }

    public function Export()
    {
      $barang = Barang_hp::all();
      Excel::create('Data Barang Habis Pakai', function($excel) use($barang){
        $excel->sheet('Data Barang Habis Pakai', function($sheet) use($barang){
          // $sheet->fromArray($barang);
          $sheet->loadView('barang_hp.export', ['barang' => $barang]);
        });
      })->export('xls');
    }

    public function Export1()
    {
      $barang = Barang_hp::all()->where('jukhir','<=','1 ');
      Excel::create('Stock Oknam', function($excel) use($barang){
        $excel->sheet('Stock Oknam', function($sheet) use($barang){
          // $sheet->fromArray($barang);
          $sheet->loadView('barang_hp.export', ['barang' => $barang]);
        });
      })->export('xls');
    }

    public function upload()
    {
      return view ('barang_hp/import');
    }

    public function import()
    {
      if(Input::hasFile('datab'))
      {
        $path = Input::file('datab')->getRealPath();
        $data = \Excel::load($path, function($reader){
        })->get();
        if(!empty($data) && $data->count())
        {
          foreach ($data as $key => $value)
          {
            $insert[] = ['naba' => $value->naba,
                        'merk' => $value->merk,
                        'koba' => $value->koba,
                        'harga' => $value->harga,
                        'tanggal' => $value->tanggal,
                        'juwal' => $value->juwal,
                        'jukhir' => $value->jukhir,
                        'ket' => $value->ket,
                        ];
          }
          if(!empty($insert))
          {
            \DB::table('barang_hps')->insert($insert);

          }
        }
        return redirect('barang/index');
      }
  }
}
