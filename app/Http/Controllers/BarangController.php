<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Barang;
use App\Barang_hp;
use App\Peminjam;
use PDF;
use Excel;
use Illuminate\Support\Facades\Input;


class BarangController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index(Barang $barang)
    {
      $barang = Barang::all();
      $bar = Barang_hp::all();
      // dd($barang);
      return view('barang/index', ['barang' => $barang, 'bar' => $bar]);
    }
    public function detail(Barang $barang, Peminjam $peminjam, $naba)
    {
      $barang = Barang::where('naba',  $naba)->get();
      $peminjam = Peminjam::where('barang', 'like', $naba."%")->where('tgl_p','')->get();
      return view('barang/detail', ['barang' => $barang, 'peminjam' => $peminjam]);
    }
    public function create()
    {
      # code...
      return view('barang/tambah');
    }
    public function store(Request $req, Barang $barang)
    {
      # code...
      $bar = new $barang;
      $bar->naba = $req->nama;
      $bar->merk = $req->merk;
      $bar->koba = $req->kode;
      $bar->harga = $req->harga;
      $bar->tanggal = $req->tgl;
      $bar->juwal = $req->awal;
      $bar->jukhir = $req->akhir;
      $bar->ket = $req->ket;

      $bar->save();

      return redirect('barang/index');
    }
    public function edit(Barang $barang, $id)
    {
      # code...
      $barang = Barang::findOrFail($id);
      return view('barang/ubah', ['barang' => $barang]);
    }

    public function update(Request $req , $id)
    {
      # code...
      $bar = Barang::findOrFail($id);
      $bar->naba = $req->nama;
      $bar->merk = $req->merk;
      $bar->koba = $req->kode;
      $bar->harga = $req->harga;
      $bar->tanggal = $req->tgl;
      $bar->juwal = $req->awal;
      $bar->jukhir = $req->akhir;
      $bar->ket = $req->ket;

      $bar->save();

      return redirect('barang/index');
    }

    public function delete(Barang $barang, $id)
    {
      # code...
      $barang = Barang::findOrFail($id);
      $barang->delete();

      return redirect('/barang/index');
    }

    public function pdf()
    {
      $barang = Barang::all();
      $pdf = PDF::loadView('barang.report', ['barang' => $barang]);
      return $pdf->stream('barang.pdf');
    }

    public function Export()
    {
      $barang = Barang::all();
      Excel::create('Data Barang Pinjam', function($excel) use($barang){
        $excel->sheet('Data Barang Pinjam', function($sheet) use($barang){
          // $sheet->fromArray($barang);
          $sheet->loadView('barang.export', ['barang' => $barang]);
        });
      })->export('xls');
    }

    public function upload()
    {
      return view ('barang/import');
    }

    public function import()
    {
      if(Input::hasFile('datab'))
      {
        $path = Input::file('datab')->getRealPath();
        $data = \Excel::load($path, function($reader){
        })->get();
        if(!empty($data) && $data->count())
        {
          foreach ($data as $key => $value)
          {
            $insert[] = ['naba' => $value->naba,
                        'merk' => $value->merk,
                        'koba' => $value->koba,
                        'harga' => $value->harga,
                        'tanggal' => $value->tanggal,
                        'juwal' => $value->juwal,
                        'jukhir' => $value->jukhir,
                        'ket' => $value->ket,
                        ];
          }
          if(!empty($insert))
          {
            \DB::table('barangs')->insert($insert);

          }
        }
      }
      return redirect('barang/index');
    }
  }
