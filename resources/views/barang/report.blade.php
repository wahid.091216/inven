<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laporan Stock Oknam</title>
    <style>
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
      }

      td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
      }

      tr:nth-child(even) {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }
    </style>
  </head>
  <body>
    <center><h3>Daftar Barang</h3>
    <h2>SMK Negeri 10 Jakarta Timur</h2></center>
    <table>
        <thead>
          <tr>
            <th>
              No
            </th>
            <th>Nama Barang</th>
            <th>Merk Barang</th>
            <th>Kode Barang</th>
            <th>Tanggal Pembelian</th>
            <th>Harga Barang</th>
            <th>Jumlah Awal</th>
            <th>Jumlah Akhir</th>
            <th>
              Keterangan
            </th>
          </tr>
        </thead>
        <tbody>@php $i=1; @endphp
        @foreach($barang as $b)
          <tr>
            <td>
              {{ $i++ }}
            </td>
            <td>{{$b->naba}}</td>
            <td>{{$b->merk}}</td>
            <td>{{$b->koba}}</td>
            <td>{{$b->tanggal}}</td>
            <td style="width:100px">{{$b->harga}}</td>
            <td>{{$b->juwal}}</td>
            <td>{{$b->jukhir}}</td>
            <td>
              {{$b->ket}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <script src="{{ asset('/js/pdf.js')}}"></script>
  </body>
</html>
